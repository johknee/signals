/*
========================================================

   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   

Constructor and variables

========================================================
*/

	var Signals = function(el,properties) {
		if(!window.jQuery)
		{
			this._speakError(101,"Requires jQuery to work, please include it before Signals is called");
			return false;
		}

		this.ready 			= false;
		this.element		= el;

		if(this.element.find("source").length)
		{
			this.src		= this.element.find("source:eq(0)").attr("src");
		}
		else
		{
			this.src		= this.element.attr("src");
		}

		if(!this.src)
		{
			this._speakError(107,"This element has no source. Are you sure its a video?");
			return false;
		}
		
		this.id 			= this.element.attr("id");

		if(!this.id)
		{
			this._generateID();
		}
		else
		{
			if($("#"+this.id).length>1)
			{
				this._speakError(106,"More than one element has this ID...what?!");
				return false;
			}
		}

		this._attachSelf(); // Adds it early

		this.url			= this._urlMatch();
		this.source 		= this._determineSource();
		this.query			= this._parseQuery(this.src);
		this.origin 		= '*';

		this.progress 		= {
			percentage : 0
		};
		this.loadProgress 	= {
			percentage : 0
		};

		this.properties	= {
			ready 			: function(){},
			playing			: function(){},
			loading			: function(){},
			finish			: function(){},
			change			: function(){},
			seek			: function(){}
		}

		$.extend(this.properties,properties);

		this._onReady();
	};


/*
========================================================

speakError
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	Signals.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

map
Returns: (float)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._map = function(value, low1, high1, low2, high2)
	{
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	};


/*
========================================================

generateID
Returns: (null)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._generateID = function()
	{
		this._speakError(102,"Your element should really have an ID when initialised");

		var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    this.id = text;
	    this.element.attr("id",text);

	    if(this.src.search("vimeo.com")>=0)
	    {
	    	this.src = this.src + "&player_id="+text;
	    	this.element.attr("src",this.src);
	    }

	    return text;
	};



/*
========================================================

toTime
Returns: (float)

Turns the time into its human readable counterpart;

========================================================
*/

	Signals.prototype.toTime = function(time)
	{
	    var sec_num = parseInt(time, 10);
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}

	    if(hours>0)
	    {
	    	if (hours   < 10) {hours   = "0"+hours;}

	    	var time    = hours+':'+minutes+':'+seconds;
	    }
	    else
	    {
	    	var time    = minutes+':'+seconds;
	    }
	    return time;
	};


/*
========================================================

determineSource (private)
Returns: (string)

Works out what kind of a video this truly is

========================================================
*/

	Signals.prototype._determineSource = function() {
		if(this.element.get(0).tagName=="VIDEO")
		{
			return "html";
		}
		if(this.url.search("vimeo")>=0)
		{
			return "vimeo";
		}
		if(this.url.search("youtube")>=0)
		{
			return "youtube";
		}
	};


/*
========================================================

urlMatch (private)
Returns: (string)

Appends the current protocol if none is there

========================================================
*/


	Signals.prototype._urlMatch = function() {

		var str = '';

		if(this.src.indexOf("?")>=0)
		{
			str = this.src.split('?')[0];
		}
		else
		{
			str = this.src;
		}

		if(str.substr(0,4)!="http")
		{
			return window.location.protocol + str;
		}
		else
		{
			return str;
		}
	};



/*
========================================================

setYoutubeCheck
Returns: (null)

Checks to see whether the youtube api is loaded before
continuing

========================================================
*/

	Signals.prototype._setYoutubeCheck = function()
	{
		var self = this;

		var retry = function()
		{
			self.ytTimeout = setTimeout(function()
			{
				self._setYoutubeCheck();
			},100);
		};

		if(typeof YT !== 'undefined')
		{
			if(YT.loaded)
			{
				this.ready = true;
				this._onReady();
			}
			else
			{
				retry();
			}
		}
		else
		{
			retry();
		}
	};


/*
========================================================

parseQuery (private)
Returns: (object)

Splits the query down into a key value object

========================================================
*/

	Signals.prototype._parseQuery = function(str)
	{
		if(str.indexOf("?")>=0&&str.split("?").length>1)
		{
			var str = str.split("?")[1];
			var parts = str.split("&");
			var obj = {};

			for(var i = 0; i < parts.length; i++)
			{
				var kv = parts[i].split("=");

				obj[kv[0]] = kv[1];
			}

			switch(this.source)
			{
				case "youtube":
					if(!('enablejsapi' in obj))
					{
						var join = "?";

						if(parts.length>0)
						{
							join = "&";
						}
						
						this.src = this.src+join+"enablejsapi=1";
						this.element.attr("src",this.src);
						this._speakError(108,"Your youtube video is meant to have the query 'enablejsapi=1' appended to it");
					}
					break;
				case "vimeo":
					if(!('api' in obj))
					{
						var join = "?";

						if(parts.length>0)
						{
							join = "&";
						}

						this.src = this.src+join+"api=1";
						this.element.attr("src",this.src);
						this._speakError(108,"Your vimeo video is meant to have the query 'api=1' appended to it");
					}
					break;
			}

			return obj;
		}
		else
		{
			switch(this.source)
			{
				case "youtube":
					this.src = this.src+"?enablejsapi=1";
					this.element.attr("src",this.src);
					this._speakError(108,"Your youtube video is meant to have the query 'enablejsapi=1' appended to it");
					break;
				case "vimeo":
					this.src = this.src+"?api=1";
					this.element.attr("src",this.src);
					this._speakError(108,"Your vimeo video is meant to have the query 'api=1' appended to it");
					break;
			}
		}
	};


/*
========================================================

attachSelf (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._attachSelf = function()
	{
		this.element.data("player",this);
		this.element.addClass("Signals-Attached");
	};


/*
========================================================

returnObject
Returns: (null)

Returns an object of data to do with the current state
of the player

========================================================
*/

	Signals.prototype.returnObject = function()
	{
		var obj = {};
		obj.progress 		= this.progress;
		obj.loadProgress 	= this.loadProgress;
		obj.url 			= this.url;
		obj.nativeUrl 		= this.nativeUrl;
		obj.duration		= this.duration;
		obj.timeDuration	= this.timeDuration;
		obj.colour 			= this.color;
		obj.state 			= this.state;
		obj.element 		= this.element;
		obj.volume 			= this.volume;
		obj.id 				= this.id;
		obj.reference 		= this;

		return obj;
	}


/*
========================================================

onReady (private)
Returns: (null)

Fires the initial posts to get certain data about the
current player

========================================================
*/

	Signals.prototype._onReady = function()
	{
		var self = this;

		switch(this.source)
		{
			case "youtube":
				if(this.ready)
				{
				    this.properties.ready(this.returnObject());
				    this.state = "ready";

				    this.ytPlayer = new YT.Player(this.id, {
						events: {
							'onReady': function(event) {
								self._ytReady(event);
							}
						}
					});
				}
				else
				{
					this._setYoutubeCheck();
				}
				break;
			case "vimeo":

				if(this.element.data("origin"))
				{
					this.origin 	= this.element.data("origin");
				}

				if(this.origin)
				{
				    this.properties.ready(this.returnObject());

				    this.state = "ready";

					this.post('getDuration');
					this.post('getColor');
					this.post('getVideoHeight');
					this.post('getVideoWidth');
					this.post('getVideoUrl');
					this.post('getVolume');
				    this.post('addEventListener', 'play');
				    this.post('addEventListener', 'pause');
				    this.post('addEventListener', 'finish');
				    this.post('addEventListener', 'playProgress');
				    this.post('addEventListener', 'loadProgress');
				    this.post('addEventListener', 'seek');
				}
				break;
		}
	};


/*
========================================================

ytReady (private)
Returns: (null)

Fires when the youtube api has registered the iframe
and is willing to talk with it

========================================================
*/

	Signals.prototype._ytReady = function()
	{
		var self = this;

		this.ytPlayer.addEventListener('onStateChange', function(event){ self._ytStateChange(event); });

		this.duration 		= this.ytPlayer.getDuration();
	    this.timeDuration 	= this.toTime(this.duration);
	    this.nativeUrl 		= this.ytPlayer.getVideoUrl();
	    this.volume 		= this.ytPlayer.getVolume();
	};



/*
========================================================

playing (private)
Returns: (null)

Fires the function for playing

========================================================
*/

	Signals.prototype._playing = function(data)
	{
	    this.progress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.playing(object);
	};



/*
========================================================

loading (private)
Returns: (null)

Fires the function for loading

========================================================
*/

	Signals.prototype._loading = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.loading(object);
	};



/*
========================================================

ytSetPlaying (private)
Returns: (null)

A fake function that recurs to emulate the loading and
playing events that vimeo has as standard

========================================================
*/

	Signals.prototype._ytSetPlaying = function(set)
	{
		var self = this;

		if(set)
		{
			this.ytTimeout = setTimeout(function()
			{
				var time = self.ytPlayer.getCurrentTime();
				var perc = time / self.duration;
				self._playing({percent:perc});

				var loadtime = self.ytPlayer.getVideoLoadedFraction();
				var perc = loadtime;
				self._loading({percent:perc});

				self._ytSetPlaying(true);
			},250);
		}
		else
		{
			clearTimeout(this.ytTimeout);
		}
	};

/*
========================================================

seek (private)
Returns: (null)

Fires the function for seeking

========================================================
*/

	Signals.prototype._seek = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.seek(object);
	};


/*
========================================================

ytStateChange (private)
Returns: (null)

Function that interprets the youtube event from a state
changing to send it where it needs to go

========================================================
*/

	Signals.prototype._ytStateChange = function(event)
	{
	    switch(event.data)
		{
			case YT.PlayerState.ENDED:
				this.state = 'finish';
				if(this.loopVar)
				{
					this.seek(0);
				}
				this._finish();
				break;
			case YT.PlayerState.PLAYING:
				this.state = 'play';
				this._stateChange(this.state);
				this._ytSetPlaying(true);
				break;
			case YT.PlayerState.PAUSED:
				this.state = 'pause';
				this._stateChange(this.state);
				this._ytSetPlaying(false);
				break;
			case YT.PlayerState.BUFFERING: // unused

				break;
			case YT.PlayerState.CUED: // unused

				break;
		}
	};


/*
========================================================

stateChange (private)
Returns: (null)

Fires the function when the state changes (play/pause)

========================================================
*/

	Signals.prototype._stateChange = function(state)
	{
	    var object = this.returnObject();
		this.properties.change(object);
	};

/*
========================================================

finish (private)
Returns: (null)

Fires the function when finished

========================================================
*/

	Signals.prototype._finish = function()
	{
	    var object = this.returnObject();
		this.properties.finish(object);
	};


/*
========================================================

onMessageReceived (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._onMessageReceived = function(e)
	{

		var data = JSON.parse(e.data);
	    
	    if(data.player_id==this.id)
	    {
		    if(data.method)
		    {
			    switch(data.method)
			    {
			        case 'getDuration':
			            this.duration = data.value;
			            this.timeDuration = this.toTime(this.duration);
			            break;
			        case 'getColor':
			            this.color = data.value;
			            break;
			        case 'getVideoWidth':
			            this.nativeWidth = data.value;
			            break;
			        case 'getVideoHeight':
			            this.nativeHeight = data.value;
			            break;
			        case 'getVideoUrl':
			            this.nativeUrl = data.value;
			            break;
			        case 'getVolume':
			            this.volume = data.value;
			            break;
			    }
		    }
		    else
		    {
			    switch(data.event)
			    {
			        case 'playProgress':
			            this._playing(data.data);
			            break;
			        case 'loadProgress':
			            this._loading(data.data);
			            break;
			        case 'seek':
			            this._seek(data.data);
			            break;
			        case 'ready':
			            this._onReady();
			            break;
			        case 'play':
			        case 'pause':
			        	this.state = data.event;
			            this._stateChange(data.event);
			            break;
			        case 'finish':
			        	this.state = data.event;
			            this._finish(data.event);
			            break;
			    }
		    }
		}
	};


/*
========================================================

post (private)
Returns: (null)

Posts messages to the player iframe

========================================================
*/

	Signals.prototype.post = function(action, value)
	{
	    var data = {
	    	method: action
	    };
	    
	    if(value) {
	        data.value = value;
	    }

	    var message = JSON.stringify(data);

	    try {
	    	this.element.get(0).contentWindow.postMessage(data, this.origin);
		}
		catch(err) {
			this._speakError(104,"There was an error posting to the iframe");
		}
	};



/*
========================================================

Interaction API
Returns: (null)

The methods in which to interact with the player

========================================================
*/

	Signals.prototype.unload = function()
	{
		switch(this.source)
		{
			case "youtube":
				this.ytPlayer.stopVideo();
				this.ytPlayer.clearVideo();
				break;
			case "vimeo":
				this.post("unload");
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.play = function()
	{
		switch(this.source)
		{
			case "youtube":
				this.ytPlayer.playVideo();
				break;
			case "vimeo":
				this.post("play");
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.pause = function()
	{
		switch(this.source)
		{
			case "youtube":
				this.ytPlayer.pauseVideo();
				break;
			case "vimeo":
				this.post("pause");
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.seek = function(seconds)
	{
		switch(this.source)
		{
			case "youtube":
				this.ytPlayer.seekTo(seconds,true);
				break;
			case "vimeo":
				this.post("seekTo",seconds);
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.setVolume = function(vol)
	{
		switch(this.source)
		{
			case "youtube":
				this.volume = vol;
				this.ytPlayer.setVolume(this.volume);
				break;
			case "vimeo":
				this.volume = (vol/100);
				this.post("setVolume",this.volume);
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.loop = function(bool)
	{
		this.loopVar = bool;
		switch(this.source)
		{
			case "youtube":

				break;
			case "vimeo":
				this.post("setLoop",bool);
				break;
			case "html":

				break;
		}
	};
	Signals.prototype.colorChange = function(colour)
	{
		switch(this.source)
		{
			case "youtube":
				this._speakError(105,"Youtube does not support changing colour");
				break;
			case "vimeo":
				this.post("setColor",colour);
				break;
			case "html":

				break;
		}
	};







/*
========================================================

   _____ _                   _       _    _      _                 
  / ____(_)                 | |     | |  | |    | |                
 | (___  _  __ _ _ __   __ _| |___  | |__| | ___| |_ __   ___ _ __ 
  \___ \| |/ _` | '_ \ / _` | / __| |  __  |/ _ \ | '_ \ / _ \ '__|
  ____) | | (_| | | | | (_| | \__ \ | |  | |  __/ | |_) |  __/ |   
 |_____/|_|\__, |_| |_|\__,_|_|___/ |_|  |_|\___|_| .__/ \___|_|   
            __/ |                                 | |              
           |___/                                  |_|              


 This class attaches itself on load, to aid specifically
 the vimeo videos work.

========================================================
*/
	
	var SignalsHelper = function() {
		this.silence 	= false;
		this.addAPI();
		this.addEvents();
	};

/*
========================================================

addAPI
Returns: (null)

Async adds the youtube api script

========================================================
*/

	SignalsHelper.prototype.addAPI = function() {
		var tag = document.createElement('script');
		tag.src = "//www.youtube.com/player_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	};

/*
========================================================

Add Events
Returns: (null)

This will add the listener events to the window once, so
that it can control and dictate any child objects

========================================================
*/

	SignalsHelper.prototype.addEvents = function() {

		if(!window.SignalsEvent)
		{
			var self = this;

			if(window.addEventListener)
			{
		        window.addEventListener('message', function(e){
		        	self._onMessageReceived(e)
		        }, false);

		        window.SignalsEvent = true;
		    }
		    else
		    {
		        window.attachEvent('onmessage', function(e){
		        	self._onMessageReceived(e);
		        }, false);

		        window.SignalsEvent = true;
		    }
		}
	};

/*
========================================================

onMessageReceived (private)
Returns: (null)

Acts as the listener for all vimeo videos

========================================================
*/

	SignalsHelper.prototype._onMessageReceived = function(event) {
		var data = JSON.parse(event.data);

		if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
            return false; // Not vimeo so ignore
        }


        var el = $("iframe#"+data.player_id);

        if(el.length=="0")
        {
        	this._speakError(103,"Cannot find an iframe with that ID");
        	return false;
        }

        if(el.data("player"))
        {
        	var player = el.data("player");

	        if(player.origin === '*') {
	            player.origin = event.origin;
	        }

	        player._onMessageReceived(event);
        }
        else
        {
        	el.data("origin",event.origin);

        }
	};

/*
========================================================

Throw Error
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	SignalsHelper.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

Add the helper to the window

========================================================
*/
	
	window.signalsHelper = new SignalsHelper();







