/*
========================================================

   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   

Constructor and variables

========================================================
*/

	var Signals = function(el,properties) {
		if(!window.jQuery)
		{
			this._speakError(101,"Requires jQuery to work, please include it before Signals is called");
			return false;
		}

		this.ready 			= false;
		this.element		= el;
		this.videoEl 		= this.element.get(0);

		if(this.element.find("source").length)
		{
			this.src		= this.element.find("source:eq(0)").attr("src");
		}
		else
		{
			this.src		= this.element.attr("src");
		}

		this._speakError(107,"This element has no source. Are you sure its a video tag?");


		this.id 			= this.element.attr("id");

		if(!this.id)
		{
			this._generateID();
		}
		else
		{
			if($("#"+this.id).length>1)
			{
				this._speakError(106,"More than one element has this ID...what?!");
				return false;
			}
		}

		this._attachSelf(); // Adds it early

		this.url			= this._urlMatch();
		this.query			= this._parseQuery(this.src);

		this.progress 		= {
			percentage : 0
		};
		this.loadProgress 	= {
			percentage : 0
		};

		this.properties	= {
			ready 			: function(){},
			playing			: function(){},
			loading			: function(){},
			finish			: function(){},
			change			: function(){},
			seek			: function(){}
		}

		$.extend(this.properties,properties);

		this._onReady();
	};


/*
========================================================

speakError
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	Signals.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

map
Returns: (float)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._map = function(value, low1, high1, low2, high2)
	{
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	};


/*
========================================================

generateID
Returns: (null)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._generateID = function()
	{
		this._speakError(102,"Your element should really have an ID when initialised");

		var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    this.id = text;
	    this.element.attr("id",text);

	    return text;
	};



/*
========================================================

toTime
Returns: (float)

Turns the time into its human readable counterpart;

========================================================
*/

	Signals.prototype.toTime = function(time)
	{
	    var sec_num = parseInt(time, 10);
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}

	    if(hours>0)
	    {
	    	if (hours   < 10) {hours   = "0"+hours;}

	    	var time    = hours+':'+minutes+':'+seconds;
	    }
	    else
	    {
	    	var time    = minutes+':'+seconds;
	    }
	    return time;
	};


/*
========================================================

parseQuery (private)
Returns: (object)

Splits the query down into a key value object

========================================================
*/

	Signals.prototype._parseQuery = function(str)
	{
		if(str.indexOf("?")>=0&&str.split("?").length>1)
		{
			var str = str.split("?")[1];
			var parts = str.split("&");
			var obj = {};

			for(var i = 0; i < parts.length; i++)
			{
				var kv = parts[i].split("=");

				obj[kv[0]] = kv[1];
			}

			return obj;
		}
	};


/*
========================================================

urlMatch (private)
Returns: (string)

Appends the current protocol if none is there

========================================================
*/


	Signals.prototype._urlMatch = function() {

		var str = '';

		if(this.src.indexOf("?")>=0)
		{
			str = this.src.split('?')[0];
		}
		else
		{
			str = this.src;
		}

		if(str.substr(0,4)!="http")
		{
			return window.location.protocol + str;
		}
		else
		{
			return str;
		}
	};


/*
========================================================

attachSelf (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._attachSelf = function()
	{
		this.element.data("player",this);
		this.element.addClass("Signals-Attached");
	};


/*
========================================================

returnObject
Returns: (null)

Returns an object of data to do with the current state
of the player

========================================================
*/

	Signals.prototype.returnObject = function()
	{
		var obj = {};
		obj.progress 		= this.progress;
		obj.loadProgress 	= this.loadProgress;
		obj.url 			= this.url;
		obj.nativeUrl 		= this.nativeUrl;
		obj.duration		= this.duration;
		obj.timeDuration	= this.timeDuration;
		obj.colour 			= this.color;
		obj.state 			= this.state;
		obj.element 		= this.element;
		obj.volume 			= this.volume;
		obj.id 				= this.id;

		return obj;
	}


/*
========================================================

onReady (private)
Returns: (null)

Fires the initial posts to get certain data about the
current player

========================================================
*/

	Signals.prototype._onReady = function()
	{
		var self = this;


	    var setData = function() {
		    self.duration 		= self.videoEl.duration;
		    self.timeDuration 	= self.toTime(self.videoEl.duration);
		    self.volume 		= self.videoEl.volume;

		    self.state = "ready";
		    self.properties.ready(self.returnObject());
	    };

	    if(this.videoEl.readyState<1)
	    {
		    this.element.on("loadedmetadata",function() {
			    setData();
		    });
		}
		else
		{
		    setData();
		}

	    this.element.on("timeupdate",function() {
	    	self._playing({
	    		percent: self.videoEl.currentTime / self.videoEl.duration
	    	});
	    });
	    this.element.on("progress",function() {
	    	self._loading({
	    		percent: self.videoEl.buffered.end(0) / self.videoEl.duration
	    	});
	    });

		this.element.on("play",function(e) {
			self.state = "play";
			self._stateChange("play");
		});

		this.element.on("pause",function(e) {
			self.state = "pause";
			self._stateChange("pause");
		});

		this.element.on("ended",function(e) {
			self._finish(self.returnObject());
			if (typeof self.videoEl.loop != 'boolean')
			{
				if(self.loopVar)
				{
					self.videoEl.seek(0);
				}
			}
		});
	};



/*
========================================================

playing (private)
Returns: (null)

Fires the function for playing

========================================================
*/

	Signals.prototype._playing = function(data)
	{
	    this.progress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.playing(object);
	};



/*
========================================================

loading (private)
Returns: (null)

Fires the function for loading

========================================================
*/

	Signals.prototype._loading = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.loading(object);
	};

/*
========================================================

seek (private)
Returns: (null)

Fires the function for seeking

========================================================
*/

	Signals.prototype._seek = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.seek(object);
	};


/*
========================================================

stateChange (private)
Returns: (null)

Fires the function when the state changes (play/pause)

========================================================
*/

	Signals.prototype._stateChange = function(state)
	{
	    var object = this.returnObject();
		this.properties.change(object);
	};

/*
========================================================

finish (private)
Returns: (null)

Fires the function when finished

========================================================
*/

	Signals.prototype._finish = function()
	{
	    var object = this.returnObject();
		this.properties.finish(object);
	};



/*
========================================================

Interaction API
Returns: (null)

The methods in which to interact with the player

========================================================
*/

	Signals.prototype.unload = function()
	{
		this.seek(0);
		this.pause();
	};
	Signals.prototype.play = function()
	{
		this.videoEl.play();
	};
	Signals.prototype.pause = function()
	{
		this.videoEl.pause();
	};
	Signals.prototype.seek = function(seconds)
	{
		this.videoEl.currentTime = seconds;
	};
	Signals.prototype.setVolume = function(vol) // NORMALISED (0 - 1)
	{
		this.volume = vol;
		this.videoEl.volume = this.volume;
	};
	Signals.prototype.loop = function(bool)
	{
		this.loopVar = bool;

		if (typeof this.videoEl.loop == 'boolean')
		{
			this.videoEl.loop = bool;
		}
	};
	Signals.prototype.colorChange = function(colour)
	{
		this.color = colour;
		this._speakError(105,"The video tag has no use for the colour method");
	};







/*
========================================================

   _____ _                   _       _    _      _                 
  / ____(_)                 | |     | |  | |    | |                
 | (___  _  __ _ _ __   __ _| |___  | |__| | ___| |_ __   ___ _ __ 
  \___ \| |/ _` | '_ \ / _` | / __| |  __  |/ _ \ | '_ \ / _ \ '__|
  ____) | | (_| | | | | (_| | \__ \ | |  | |  __/ | |_) |  __/ |   
 |_____/|_|\__, |_| |_|\__,_|_|___/ |_|  |_|\___|_| .__/ \___|_|   
            __/ |                                 | |              
           |___/                                  |_|              


 This class attaches itself on load, to aid specifically
 the vimeo videos work.

========================================================
*/
	
	var SignalsHelper = function() {
		this.silence 	= false;
	};

/*
========================================================

Throw Error
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	SignalsHelper.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

Add the helper to the window

========================================================
*/
	
	window.signalsHelper = new SignalsHelper();







