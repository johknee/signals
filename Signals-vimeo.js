/*
========================================================

   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   

Constructor and variables

========================================================
*/

	var Signals = function(el,properties) {
		if(!window.jQuery)
		{
			this._speakError(101,"Video Wrapper requies jquery to work");
			return false;
		}

		this.element		= el;
		this.src			= this.element.attr('src');
		this.id 			= this.element.attr("id");

		if(!this.id)
		{
			this._generateID();
		}

		this._attachSelf(); // Adds it early

		this.url			= window.location.protocol + this.src.split('?')[0];
		this.query			= this._parseQuery(this.src.split('?')[1]);
		this.origin 		= "*";

		this.progress 		= {
			percentage : 0
		};
		this.loadProgress 	= {
			percentage : 0
		};

		this.properties	= {
			ready 			: function(){},
			playing			: function(){},
			loading			: function(){},
			finish			: function(){},
			change			: function(){},
			seek			: function(){}
		}

		$.extend(this.properties,properties);

		if(this.element.data("origin"))
		{
			this.origin 	= this.element.data("origin");
			this._onReady();
		}
	};


/*
========================================================

speakError
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	Signals.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

map
Returns: (float)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._map = function(value, low1, high1, low2, high2)
	{
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	};


/*
========================================================

generateID
Returns: (null)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._generateID = function()
	{
		this._speakError(102,"Your element should really have an ID when initialised");

		var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    if(this.src.split('?').length>1)
	    {
		    this.src += "&player_id="+text;
	    }
	    else
	    {
	    	this.src += "?player_id="+text;
	    }

	    this.id = text;
	    this.element.attr("src",this.src);
	    this.element.attr("id",text);

	    return text;
	};



/*
========================================================

toTime
Returns: (float)

Turns the time into its human readable counterpart;

========================================================
*/

	Signals.prototype.toTime = function(time)
	{
	    var sec_num = parseInt(time, 10);
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}

	    if(hours>0)
	    {
	    	if (hours   < 10) {hours   = "0"+hours;}

	    	var time    = hours+':'+minutes+':'+seconds;
	    }
	    else
	    {
	    	var time    = minutes+':'+seconds;
	    }
	    return time;
	};



/*
========================================================

parseQuery (private)
Returns: (object)

Splits the query down into a key value object

========================================================
*/

	Signals.prototype._parseQuery = function(str)
	{
		var parts = str.split("&");
		var obj = {};

		for(var i = 0; i < parts.length; i++)
		{
			var kv = parts[i].split("=");

			obj[kv[0]] = kv[1];
		}

		return obj;
	};


/*
========================================================

attachSelf (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._attachSelf = function()
	{
		this.element.data("player",this);
		this.element.addClass("Signals-Attached");
	};


/*
========================================================

returnObject
Returns: (null)

Returns an object of data to do with the current state
of the player

========================================================
*/

	Signals.prototype.returnObject = function()
	{
		var obj = {};
		obj.progress 		= this.progress;
		obj.loadProgress 	= this.loadProgress;
		obj.url 			= this.url;
		obj.nativeUrl 		= this.nativeUrl;
		obj.duration		= this.duration;
		obj.timeDuration	= this.timeDuration;
		obj.colour 			= this.color;
		obj.state 			= this.state;
		obj.element 		= this.element;
		obj.volume 			= this.volume*100;
		obj.id 				= this.id;
		obj.reference 		= this;

		return obj;
	}


/*
========================================================

onReady (private)
Returns: (null)

Fires the initial posts to get certain data about the
current player

========================================================
*/

	Signals.prototype._onReady = function()
	{
	    this.properties.ready(this.returnObject());

	    this.state = "ready";

		this.post('getDuration');
		this.post('getColor');
		this.post('getVideoHeight');
		this.post('getVideoWidth');
		this.post('getVideoUrl');
		this.post('getVolume');
	    this.post('addEventListener', 'play');
	    this.post('addEventListener', 'pause');
	    this.post('addEventListener', 'finish');
	    this.post('addEventListener', 'playProgress');
	    this.post('addEventListener', 'loadProgress');
	    this.post('addEventListener', 'seek');
	};



/*
========================================================

playing (private)
Returns: (null)

Fires the function for playing

========================================================
*/

	Signals.prototype._playing = function(data)
	{
	    this.progress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.playing(object);
	};

/*
========================================================

loading (private)
Returns: (null)

Fires the function for loading

========================================================
*/

	Signals.prototype._loading = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.loading(object);
	};

/*
========================================================

seek (private)
Returns: (null)

Fires the function for seeking

========================================================
*/

	Signals.prototype._seek = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.seek(object);
	};


/*
========================================================

stateChange (private)
Returns: (null)

Fires the function when the state changes (play/pause)

========================================================
*/

	Signals.prototype._stateChange = function(state)
	{
	    var object = this.returnObject();
		this.properties.change(object);
	};

/*
========================================================

finish (private)
Returns: (null)

Fires the function when finished

========================================================
*/

	Signals.prototype._finish = function()
	{
	    var object = this.returnObject();
		this.properties.finish(object);
	};


/*
========================================================

onMessageReceived (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._onMessageReceived = function(e)
	{

		var data = JSON.parse(e.data);
	    
	    if(data.player_id==this.id)
	    {
		    if(data.method)
		    {
			    switch(data.method)
			    {
			        case 'getDuration':
			            this.duration = data.value;
			            this.timeDuration = this.toTime(this.duration);
			            break;
			        case 'getColor':
			            this.color = data.value;
			            break;
			        case 'getVideoWidth':
			            this.nativeWidth = data.value;
			            break;
			        case 'getVideoHeight':
			            this.nativeHeight = data.value;
			            break;
			        case 'getVideoUrl':
			            this.nativeUrl = data.value;
			            break;
			        case 'getVolume':
			            this.volume = data.value;
			            break;
			    }
		    }
		    else
		    {
			    switch(data.event)
			    {
			        case 'playProgress':
			            this._playing(data.data);
			            break;
			        case 'loadProgress':
			            this._loading(data.data);
			            break;
			        case 'seek':
			            this._seek(data.data);
			            break;
			        case 'ready':
			            this._onReady();
			            break;
			        case 'play':
			        case 'pause':
			        	this.state = data.event;
			            this._stateChange(data.event);
			            break;
			        case 'finish':
			        	this.state = data.event;
			            this._finish(data.event);
			            break;
			    }
		    }
		}
	};


/*
========================================================

post (private)
Returns: (null)

Posts messages to the player iframe

========================================================
*/

	Signals.prototype.post = function(action, value)
	{
	    var data = {
	    	method: action
	    };
	    
	    if(value) {
	        data.value = value;
	    }

	    var message = JSON.stringify(data);

	    try {
	    	this.element.get(0).contentWindow.postMessage(data, this.origin);
		}
		catch(err) {
			this._speakError(104,"There was an error posting to the iframe");
		}
	};




/*
========================================================

Interaction API
Returns: (null)

The methods in which to interact with the player

========================================================
*/

	Signals.prototype.unload = function()
	{
		this.post("unload");
	};
	Signals.prototype.play = function()
	{
		this.post("play");
	};
	Signals.prototype.pause = function()
	{
		this.post("pause");
	};
	Signals.prototype.seek = function(seconds)
	{
		this.post("seekTo",seconds);
	};
	Signals.prototype.setVolume = function(vol)
	{
		this.volume = (vol/100);
		this.post("setVolume",this.volume);
	};
	Signals.prototype.loop = function(bool)
	{
		this.loopVar = bool;
		this.post("setLoop",bool);
	};
	Signals.prototype.colorChange = function(colour)
	{
		this.post("setColor",colour);
	};







/*
========================================================

   _____ _                   _       _    _      _                 
  / ____(_)                 | |     | |  | |    | |                
 | (___  _  __ _ _ __   __ _| |___  | |__| | ___| |_ __   ___ _ __ 
  \___ \| |/ _` | '_ \ / _` | / __| |  __  |/ _ \ | '_ \ / _ \ '__|
  ____) | | (_| | | | | (_| | \__ \ | |  | |  __/ | |_) |  __/ |   
 |_____/|_|\__, |_| |_|\__,_|_|___/ |_|  |_|\___|_| .__/ \___|_|   
            __/ |                                 | |              
           |___/                                  |_|              


 This class attaches itself on load, to aid specifically
 the vimeo videos work.

========================================================
*/
	
	var SignalsHelper = function() {
		this.silence 	= false;
		this.addEvents();
	};

/*
========================================================

Throw Error
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	SignalsHelper.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};

/*
========================================================

Add Events
Returns: (null)

This will add the listener events to the window once, so
that it can control and dictate any child objects

========================================================
*/

	SignalsHelper.prototype.addEvents = function() {

		if(!window.SignalsEvent)
		{
			var self = this;

			if(window.addEventListener)
			{
		        window.addEventListener('message', function(e){
		        	self._onMessageReceived(e)
		        }, false);

		        window.SignalsEvent = true;
		    }
		    else
		    {
		        window.attachEvent('onmessage', function(e){
		        	self._onMessageReceived(e);
		        }, false);

		        window.SignalsEvent = true;
		    }
		}
	};

/*
========================================================

Add Events
Returns: (null)

This will add the listener events to the window once, so
that it can control and dictate any child objects

========================================================
*/

	SignalsHelper.prototype._onMessageReceived = function(event) {
		var data = JSON.parse(event.data);

		if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
            return false; // Not vimeo so ignore
        }

        var el = $("iframe#"+data.player_id);

        if(el.length=="0")
        {
        	this._speakError(103,"Cannot find an iframe with that ID");
        	return false;
        }

        if(el.data("player"))
        {
        	var player = el.data("player");

	        if(player.origin === '*') {
	            player.origin = event.origin;
	        }

	        player._onMessageReceived(event);
        }
        else
        {
        	el.data("origin",event.origin);
        }
	};


/*
========================================================

Add the helper to the window

========================================================
*/
	
	window.signalsHelper = new SignalsHelper();







