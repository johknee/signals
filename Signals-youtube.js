/*
========================================================

   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   

Constructor and variables

========================================================
*/

	var Signals = function(el,properties) {
		if(!window.jQuery)
		{
			this._speakError(101,"Requires jQuery to work, please include it before Signals is called");
			return false;
		}

		this.ready 			= false;
		this.element		= el;
		this.src			= this.element.attr('src');
		this.id 			= this.element.attr("id");

		if(!this.id)
		{
			this._generateID();
		}
		else
		{
			if($("#"+this.id).length>1)
			{
				this._speakError(106,"More than one element has this ID...what?!");
				return false;
			}
		}

		this._attachSelf(); // Adds it early

		this.url			= window.location.protocol + this.src.split('?')[0];
		this.query			= this._parseQuery(this.src.split('?')[1]);
		this.origin 		= "*";

		this.progress 		= {
			percentage : 0
		};
		this.loadProgress 	= {
			percentage : 0
		};

		this.properties	= {
			ready 			: function(){},
			playing			: function(){},
			loading			: function(){},
			finish			: function(){},
			change			: function(){},
			seek			: function(){}
		}

		$.extend(this.properties,properties);

		this._onReady();
	};


/*
========================================================

speakError
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	Signals.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

map
Returns: (float)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._map = function(value, low1, high1, low2, high2)
	{
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	};


/*
========================================================

generateID
Returns: (null)

Works out the value mapped against a different range

========================================================
*/

	Signals.prototype._generateID = function()
	{
		this._speakError(102,"Your element should really have an ID when initialised");

		var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }

	    this.id = text;
	    this.element.attr("id",text);

	    return text;
	};



/*
========================================================

toTime
Returns: (float)

Turns the time into its human readable counterpart;

========================================================
*/

	Signals.prototype.toTime = function(time)
	{
	    var sec_num = parseInt(time, 10);
	    var hours   = Math.floor(sec_num / 3600);
	    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	    if (minutes < 10) {minutes = "0"+minutes;}
	    if (seconds < 10) {seconds = "0"+seconds;}

	    if(hours>0)
	    {
	    	if (hours   < 10) {hours   = "0"+hours;}

	    	var time    = hours+':'+minutes+':'+seconds;
	    }
	    else
	    {
	    	var time    = minutes+':'+seconds;
	    }
	    return time;
	};



/*
========================================================

setYoutubeCheck
Returns: (null)

Checks to see whether the youtube api is loaded before
continuing

========================================================
*/

	Signals.prototype._setYoutubeCheck = function()
	{
		var self = this;

		var retry = function()
		{
			self.ytTimeout = setTimeout(function()
			{
				self._setYoutubeCheck();
			},100);
		};

		if(typeof YT !== 'undefined')
		{
			if(YT.loaded)
			{
				this.ready = true;
				this._onReady();
			}
			else
			{
				retry();
			}
		}
		else
		{
			retry();
		}
	};


/*
========================================================

parseQuery (private)
Returns: (object)

Splits the query down into a key value object

========================================================
*/

	Signals.prototype._parseQuery = function(str)
	{
		var parts = str.split("&");
		var obj = {};

		for(var i = 0; i < parts.length; i++)
		{
			var kv = parts[i].split("=");

			obj[kv[0]] = kv[1];
		}

		return obj;
	};


/*
========================================================

attachSelf (private)
Returns: (null)

Attaches the data element of the iframe

========================================================
*/

	Signals.prototype._attachSelf = function()
	{
		this.element.data("player",this);
		this.element.addClass("Signals-Attached");
	};


/*
========================================================

returnObject
Returns: (null)

Returns an object of data to do with the current state
of the player

========================================================
*/

	Signals.prototype.returnObject = function()
	{
		var obj = {};
		obj.progress 		= this.progress;
		obj.loadProgress 	= this.loadProgress;
		obj.url 			= this.url;
		obj.nativeUrl 		= this.nativeUrl;
		obj.duration		= this.duration;
		obj.timeDuration	= this.timeDuration;
		obj.colour 			= this.color;
		obj.state 			= this.state;
		obj.element 		= this.element;
		obj.volume 			= this.volume;
		obj.id 				= this.id;
		obj.reference 		= this;

		return obj;
	}


/*
========================================================

onReady (private)
Returns: (null)

Fires the initial posts to get certain data about the
current player

========================================================
*/

	Signals.prototype._onReady = function()
	{
		var self = this;

		if(this.ready)
		{
		    this.properties.ready(this.returnObject());
		    this.state = "ready";

		    this.ytPlayer = new YT.Player(this.id, {
				events: {
					'onReady': function(event) {
						self._ytReady(event);
					}
				}
			});
		}
		else
		{
			this._setYoutubeCheck();
		}
	};


/*
========================================================

ytReady (private)
Returns: (null)

Fires when the youtube api has registered the iframe
and is willing to talk with it

========================================================
*/

	Signals.prototype._ytReady = function()
	{
		var self = this;

		this.ytPlayer.addEventListener('onStateChange', function(event){ self._ytStateChange(event); });

		this.duration 		= this.ytPlayer.getDuration();
	    this.timeDuration 	= this.toTime(this.duration);
	    this.nativeUrl 		= this.ytPlayer.getVideoUrl();
	    this.volume 		= this.ytPlayer.getVolume();
	};



/*
========================================================

playing (private)
Returns: (null)

Fires the function for playing

========================================================
*/

	Signals.prototype._playing = function(data)
	{
	    this.progress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.playing(object);
	};



/*
========================================================

loading (private)
Returns: (null)

Fires the function for loading

========================================================
*/

	Signals.prototype._loading = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.loading(object);
	};



/*
========================================================

ytSetPlaying (private)
Returns: (null)

A fake function that recurs to emulate the loading and
playing events that vimeo has as standard

========================================================
*/

	Signals.prototype._ytSetPlaying = function(set)
	{
		var self = this;

		if(set)
		{
			this.ytTimeout = setTimeout(function()
			{
				var time = self.ytPlayer.getCurrentTime();
				var perc = time / self.duration;
				self._playing({percent:perc});

				var loadtime = self.ytPlayer.getVideoLoadedFraction();
				var perc = loadtime;
				self._loading({percent:perc});

				self._ytSetPlaying(true);
			},250);
		}
		else
		{
			clearTimeout(this.ytTimeout);
		}
	};

/*
========================================================

seek (private)
Returns: (null)

Fires the function for seeking

========================================================
*/

	Signals.prototype._seek = function(data)
	{
	    this.loadProgress.percentage = data.percent;

	    var object = this.returnObject();
	    this.properties.seek(object);
	};


/*
========================================================

ytStateChange (private)
Returns: (null)

Function that interprets the youtube event from a state
changing to send it where it needs to go

========================================================
*/

	Signals.prototype._ytStateChange = function(event)
	{
	    switch(event.data)
		{
			case YT.PlayerState.ENDED:
				this.state = 'finish';
				if(this.loopVar)
				{
					this.seek(0);
				}
				this._finish();
				break;
			case YT.PlayerState.PLAYING:
				this.state = 'play';
				this._stateChange(this.state);
				this._ytSetPlaying(true);
				break;
			case YT.PlayerState.PAUSED:
				this.state = 'pause';
				this._stateChange(this.state);
				this._ytSetPlaying(false);
				break;
			case YT.PlayerState.BUFFERING: // unused

				break;
			case YT.PlayerState.CUED: // unused

				break;
		}
	};


/*
========================================================

stateChange (private)
Returns: (null)

Fires the function when the state changes (play/pause)

========================================================
*/

	Signals.prototype._stateChange = function(state)
	{
	    var object = this.returnObject();
		this.properties.change(object);
	};

/*
========================================================

finish (private)
Returns: (null)

Fires the function when finished

========================================================
*/

	Signals.prototype._finish = function()
	{
	    var object = this.returnObject();
		this.properties.finish(object);
	};



/*
========================================================

Interaction API
Returns: (null)

The methods in which to interact with the player

========================================================
*/

	Signals.prototype.unload = function()
	{
		this.ytPlayer.stopVideo();
		this.ytPlayer.clearVideo();
	};
	Signals.prototype.play = function()
	{
		this.ytPlayer.playVideo();
	};
	Signals.prototype.pause = function()
	{
		this.ytPlayer.pauseVideo();
	};
	Signals.prototype.seek = function(seconds)
	{
		this.ytPlayer.seekTo(seconds,true);
	};
	Signals.prototype.setVolume = function(vol)
	{
		this.volume = vol;
		this.ytPlayer.setVolume(this.volume);
	};
	Signals.prototype.loop = function(bool)
	{
		this.loopVar = bool;
	};
	Signals.prototype.colorChange = function(colour)
	{
		this._speakError(105,"Youtube does not support changing colour");
	};







/*
========================================================

   _____ _                   _       _    _      _                 
  / ____(_)                 | |     | |  | |    | |                
 | (___  _  __ _ _ __   __ _| |___  | |__| | ___| |_ __   ___ _ __ 
  \___ \| |/ _` | '_ \ / _` | / __| |  __  |/ _ \ | '_ \ / _ \ '__|
  ____) | | (_| | | | | (_| | \__ \ | |  | |  __/ | |_) |  __/ |   
 |_____/|_|\__, |_| |_|\__,_|_|___/ |_|  |_|\___|_| .__/ \___|_|   
            __/ |                                 | |              
           |___/                                  |_|              


 This class attaches itself on load, to aid specifically
 the vimeo videos work.

========================================================
*/
	
	var SignalsHelper = function() {
		this.silence 	= false;
		this.addAPI();
	};

/*
========================================================

addAPI
Returns: (null)

Async adds the youtube api script

========================================================
*/

	SignalsHelper.prototype.addAPI = function() {
		var tag = document.createElement('script');
		tag.src = "//www.youtube.com/player_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	};

/*
========================================================

Throw Error
Returns: (null)

Pass a string and a code and it will log out the error

========================================================
*/

	SignalsHelper.prototype._speakError = function(code,str) {
		if(!signalsHelper.silence)
		{
			console.log("SIGNALS ERR #"+code+": "+str);
		}
	};


/*
========================================================

Add the helper to the window

========================================================
*/
	
	window.signalsHelper = new SignalsHelper();







